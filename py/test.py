#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is the test example
"""
__author__ = "Le Quellec Jean"
__copyright__ = "Copyright 2018, Jean Le Quellec"
__credits__ = ["Jean Le Quellec"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = "Jean Le Quellec"
__email__ = "jean@lequellec.xyz"
__status__ = "Prototype"

print("Test Py By Param!")