<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;
use \Grav\Common\Grav;
use \Grav\Common\Page\Page;

/**
 * Class PylaunchPlugin
 * @package Grav\Plugin
 */
class PylaunchPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        //require
        //require_once(__DIR__.'/classes/Script.php');

        return [
            
            'onPluginsInitialized' => ['onPluginsInitialized', 0],         
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        $this->config = $this->grav['config'];

        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        $this->enable([
            // Required Add Content to page
            'onThemeInitialized'        => ['onThemeInitialized', 0],
        ]);
    }
    /**
     * load the JS 
     */
    public function onThemeInitialized()
    {
        // Add Needed header
        $this->enable([
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0],
            'onTwigExtensions' => ['onTwigExtensions', 0],
        ]);
    }

    public function onTwigSiteVariables()
    {
        
        $header_bits = [];
        $header_bits[] = "plugin://pylaunch/api/api.js";
        $assets = $this->grav['assets'];
        $assets->registerCollection('pylaunchjs', $header_bits);
        $assets->add('pylaunchjs', 100);
    }

    public function onTwigExtensions()
    {
        require_once(__DIR__ . '/twig/PylaunchTwigExtension.php');
        $this->grav['twig']->twig->addExtension(new PylaunchTwigExtension());
    }

}
