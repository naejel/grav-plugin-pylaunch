<?php
namespace Grav\Plugin;
class PylaunchTwigExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'PylaunchTwigExtension';
    }
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('pyscripts', [$this, 'listPyScripts']),
            new \Twig_SimpleFunction('test', [$this, 'test']),
            new \Twig_SimpleFunction('execscript', [$this, 'execScript'])

        ];
    }
    public function test($test)
    {
        return $test;
    }

    public function listPyScripts($test)
    {
        return $test;
    }

    public function ExecScript($scriptname)
    {
        $command = escapeshellcmd('../py/'.$scriptname.'.py');
        $output = shell_exec($command);
        return $output;
    }
}